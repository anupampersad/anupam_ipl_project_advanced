// Requiring Dependencies
const fs = require("fs");
const csvtojson = require("csvtojson");
const path = require('path')

// Importing Fuctions
const matchesPerYear = require('./matchesPerYear');
const matchesWonPerTeamPerYear = require('./matchesWonPerTeamPerYear');
const extraRunsConceded2016 = require('./extraRunsConceded2016');
const topEcoBowler2015 = require('./topEcoBowler2015');

// Defining Paths 
const matchesPath = path.join(__dirname, '../data/matches.csv');
const deliveriesPath = path.join(__dirname, '../data/deliveries.csv');
const outputPath = path.join(__dirname,'../public/output')


const saveAsJson = (fileName,object) =>{
    fs.writeFile(`${outputPath}/${fileName}.json`, JSON.stringify(object),'utf-8',(err)=>{
        if (err) console.log(err)
    })
}


// IIFE function to call all the pre-declared functions and save the resuls in /public/output
(async function final(){

    const matches = await csvtojson().fromFile(matchesPath);
    const deliveries = await csvtojson().fromFile(deliveriesPath);


    let matchesPerYearResult = matchesPerYear(matches);
    // console.log(matchesPerYearResult);
    saveAsJson('matchesPerYear',matchesPerYearResult);

    let matchesWonPerTeamPerYearResult = matchesWonPerTeamPerYear(matches);
    // console.log(matchesWonPerTeamPerYearResult);
    saveAsJson('matchesWonPerTeamPerYear',matchesWonPerTeamPerYearResult);

    let extraRunsConceded2016Result = extraRunsConceded2016(matches,deliveries);
    // console.log(extraRunsConceded2016Result);
    saveAsJson('extraRunsConceded2016',extraRunsConceded2016Result);

    let topEcoBowler2015Result = topEcoBowler2015(matches,deliveries);
    // console.log(topEcoBowler2015Result);
    saveAsJson('topEcoBowler2015',topEcoBowler2015Result);

})()



