function extraRunsConceded2016(matches, deliveries) {

    let matchID2016 = matches.filter((match) => {
        if (match.season == 2016) {
            return match
        }
    }).map((match) => {
        return match.id
    })

    let extraRuns2016 = deliveries.reduce((extraRuns2016Object, delivery) => {

        if (matchID2016.includes(delivery.match_id)) {

            if (delivery.bowling_team in extraRuns2016Object) {

                extraRuns2016Object[delivery.bowling_team] += parseInt(delivery.extra_runs)
            }
            else {

                extraRuns2016Object[delivery.bowling_team] = parseInt(delivery.extra_runs)
            }
        }

        return extraRuns2016Object

    }, {} )

    return extraRuns2016

}

module.exports = extraRunsConceded2016
