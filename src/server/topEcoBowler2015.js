function topEcoBowlers2015(matches, deliveries) {

    let matchID2015 = matches.filter((match) => {
        if (match.season == 2015) {
            return match
        }
    }).map((match) => {
        return match.id
    })

    let topEcoBowlers = deliveries.reduce((topEcoBowlers, delivery) => {

        if (matchID2015.includes(delivery.match_id)) {

            if (delivery.bowler in topEcoBowlers) {

                topEcoBowlers[delivery.bowler]['runs'] += parseInt(delivery.total_runs)
                topEcoBowlers[delivery.bowler]['balls'] += 1
                topEcoBowlers[delivery.bowler]['overs'] = parseInt(topEcoBowlers[delivery.bowler]['balls'] / 6) + (topEcoBowlers[delivery.bowler]['balls'] % 6) / 10
                topEcoBowlers[delivery.bowler]['economy'] = parseFloat(topEcoBowlers[delivery.bowler]['runs'] / topEcoBowlers[delivery.bowler]['overs']).toPrecision(2)

            }
            else {

                topEcoBowlers[delivery.bowler] = {}
                topEcoBowlers[delivery.bowler]['runs'] = parseInt(delivery.total_runs)
                topEcoBowlers[delivery.bowler]['balls'] = 1
                topEcoBowlers[delivery.bowler]['overs'] = parseInt(topEcoBowlers[delivery.bowler]['balls'] / 6) + (topEcoBowlers[delivery.bowler]['balls'] % 6) / 10
                topEcoBowlers[delivery.bowler]['economy'] = parseFloat(topEcoBowlers[delivery.bowler]['runs'] / topEcoBowlers[delivery.bowler]['overs']).toPrecision(2)
            }
        }

        return topEcoBowlers

    }, {} )

    // console.log(topEcoBowlers)

    // Lower economy better player because average of runs given by baller is less
    // hence we need to sort this object according to economy in ascending order 
    // and find top 10 ballers 

    const arrayFromObject = Object.entries(topEcoBowlers)
    const sortedArray = arrayFromObject.sort((x, y) => x[1].economy - y[1].economy)
    const sortedArrayTop10 = sortedArray.slice(0, 10)
    const topEconomicBowler2015 = Object.fromEntries(sortedArrayTop10)

    return topEconomicBowler2015

}

module.exports = topEcoBowlers2015
