function matchesWonPerTeamPerYear(matches) {

    let matchesWon = matches.reduce((matchesWonObject,match)=>{

        if (match.season in matchesWonObject) {

            if (match.winner in matchesWonObject[match.season]) {
                matchesWonObject[match.season][match.winner] += 1
            }
            else {
                matchesWonObject[match.season][match.winner] = 1
            }
        }

        else {

            matchesWonObject[match.season] = {}
            matchesWonObject[match.season][match.winner] = 1

        }

        return matchesWonObject

    },{})


    return matchesWon

}

module.exports = matchesWonPerTeamPerYear