function matchesPerYear(matches) {

    let matchesPerYear = matches.reduce((matchesPerYearObject,match)=>{

        if (match.season in matchesPerYearObject) {

            matchesPerYearObject[match.season] += 1
        }
        else {
            matchesPerYearObject[match.season] = 1
        }
        
        return matchesPerYearObject

    }, {})

    return matchesPerYear
}

module.exports = matchesPerYear
